<?php

/*
-----------------------------------------------------------
FILE NAME: getEducationTest.php

Copyright (c) 2015 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Erin Mills

DESCRIPTION:
This php class is used to test the GET method of the education service.

ENVIRONMENT DEPENDENCIES:
RESTng Framework
PHPUnit
Person/Education Service

TABLE USAGE:

Web Service Usage:
	Person/Education service (GET)

AUDIT TRAIL:

DATE    PRJ-TSK          UniqueID
Description:

10/14/2015               MILLSE
Description:  Initial Draft

-----------------------------------------------------------
 */
namespace MiamiOH\PhpEducationService\Tests\Unit;

class GetEducationTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $education, $user, $dbh;

    /**
     * This setUp method is run before every test method.
     * It sets up a mock API, DBH and DB object with methods that the education class uses
     * and injects those into the Education object.
     */
    protected function setUp()
    {

        //Create the mock API object and define its newResponse method
        $api = $this->getMockBuilder('\MiamiOH\RESTng\Util\API')
            //->setMethods(array('newResponse', 'callResource', 'locationFor'))
            ->setMethods(array('newResponse'))
            ->getMock();
        $api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();

        //set up the mock database object and its getHandle method
        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();
        $db->method('getHandle')->willReturn($this->dbh);

        //set up the mock user
        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthorized'))
            ->getMock();

        //Create the education object we will be unit testing and pass it the mock objects
        $this->education = new \MiamiOH\PhpEducationService\Services\Education();
        $this->education->setDatabase($db);
        $this->education->setApiUser($this->user);

    }

    public function testGetEducationSinglePidm()
    {

        /*** set up to call the getEducation method ***/
        //Create the mock request object, define it's getOptions method and pass it into the education object.
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();
        $request->expects($this->once())->method('getOptions')->willReturn(array('pidm' => array($this->mockDbRecordsSinglePidm[0]['apradeg_pidm']), 'token' => 'blahblah'));
        $this->education->setRequest($request);

        //define what we want the isAuthorized method to return for this method
        $this->user->method('isAuthorized')
            ->will($this->returnCallback(array($this, 'authorizedUser')));
        //->will($this->returnCallback(array($this, 'notAuthorizedUser'))); //for unauthorized user testing

        //define the querry_all method for this test
        $this->dbh->method('queryall_array')
            ->willReturn($this->mockDbRecordsSinglePidm);

        /*** Call the getEmail and get the payload ***/
        $resp = $this->education->getEducation();
        $payload = $resp->getPayload();

        /*** Make assertions ***/
        //check status and general response
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus()); //we expect the status to be API_OK
        $this->assertTrue(is_array($payload)); //we expect the payload to be an array

        //check payload
        $this->assertEquals($this->mockDbRecordsSinglePidm[0]['apradeg_pidm'], $payload[0]['pidm']);
        $this->assertEquals($this->mockDbRecordsSinglePidm[0]['apradeg_sbgi_code'], $payload[0]['schoolCode']);
        $this->assertEquals($this->mockDbRecordsSinglePidm[0]['apradeg_degc_code'], $payload[0]['degreeCode']);
    //    $this->assertEquals($this->mockDbRecordsSinglePidm[0]['apradeg_date'], $payload[0]['degreeDate']);

        $this->assertEquals($this->mockDbRecordsSinglePidm[0]['apradeg_user'], $payload[0]['user']);

        $this->assertEquals($this->mockDbRecordsSinglePidm[1]['apradeg_pidm'], $payload[1]['pidm']);
    }

    public function testGetEducationNoRecords()
    {
        /*** set up to call the getEducation method ***/
        //Create the mock request object, define it's getOptions method and pass it into the education object.
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();
        $request->expects($this->once())->method('getOptions')->willReturn(array('pidm' => array($this->mockDbRecordsSinglePidm[0]['apradeg_pidm']), 'token' => 'blahblah'));
        $this->education->setRequest($request);

        //define what we want the isAuthorized method to return for this method
        $this->user->method('isAuthorized')
            ->will($this->returnCallback(array($this, 'authorizedUser')));
        //->will($this->returnCallback(array($this, 'notAuthorizedUser'))); //for unauthorized user testing

        //define the querry_all method for this test
        $this->dbh->method('queryall_array')
            ->willReturn(array());

        /*** Call the getEmail and get the payload ***/
        $resp = $this->education->getEducation();
        $payload = $resp->getPayload();

        /*** Make assertions ***/
        //check status and general response
        $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus()); //we expect the status to be API_OK
        $this->assertTrue(is_array($payload)); //we expect the payload to be an array
        $this->assertTrue(empty($payload)); //we expect the payload to be empty
    }

    public function testGetEducationInvalidToken()
    {
        /*** set up to call the getEducation method ***/
        //Create the mock request object, define it's getOptions method and pass it into the education object.
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();
        $request->expects($this->once())->method('getOptions')->willReturn(array('pidm' => array($this->mockDbRecordsSinglePidm[0]['apradeg_pidm']), 'token' => 'blahblah'));
        $this->education->setRequest($request);

        //define what we want the isAuthorized method to return for this method
        $this->user->method('isAuthorized')
            //->will($this->returnCallback(array($this, 'authorizedUser'))); //for an authorized user
            ->will($this->returnCallback(array($this, 'notAuthorizedUser'))); //for unauthorized user testing

        /*** Call the getEmail and get the payload ***/
        $resp = $this->education->getEducation();

        /*** Make assertions ***/
        $this->assertEquals(\MiamiOH\RESTng\App::API_UNAUTHORIZED, $resp->getStatus());
    }

    public function testGetEducationEmptyPidm()
    {
        /*** set up to call the getEducation method ***/
        //Create the mock request object, define it's getOptions method and pass it into the education object.
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();
        $request->expects($this->once())->method('getOptions')->willReturn(array('pidm' => '', 'token' => 'blahblah'));
        $this->education->setRequest($request);

        //define what we want the isAuthorized method to return for this method
        $this->user->method('isAuthorized')
            ->will($this->returnCallback(array($this, 'authorizedUser'))); //for an authorized user
        //->will($this->returnCallback(array($this, 'notAuthorizedUser'))); //for unauthorized user testing

        /*** Call the getEmail and get the payload ***/
        try {
            $resp = $this->education->getEducation();
        } catch (\Exception $e) {
            $this->assertEquals($e->getMessage(), 'Error getting options or parameter: No pidms or uniqueids are available.');
        }
    }

    public function testGetEducationEmptyPidmArray()
    {
        /*** set up to call the getEducation method ***/
        //Create the mock request object, define it's getOptions method and pass it into the education object.
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();
        $request->expects($this->once())->method('getOptions')->willReturn(array('pidm' => array(), 'token' => 'blahblah'));
        $this->education->setRequest($request);

        //define what we want the isAuthorized method to return for this method
        $this->user->method('isAuthorized')
            ->will($this->returnCallback(array($this, 'authorizedUser'))); //for an authorized user
        //->will($this->returnCallback(array($this, 'notAuthorizedUser'))); //for unauthorized user testing

        /*** Call the getEmail and get the payload ***/
        try {
            $resp = $this->education->getEducation();
        } catch (\Exception $e) {
            $this->assertEquals($e->getMessage(), 'Error getting options or parameter: No pidms or uniqueids are available.');
        }
    }

    public function testGetEducationBadPidm()
    {
        /*** set up to call the getEducation method ***/

        $badPidm = '112x90';

        //Create the mock request object, define it's getOptions method and pass it into the education object.
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();
        $request->expects($this->once())->method('getOptions')->willReturn(array('pidm' => array($badPidm), 'token' => 'blahblah'));
        $this->education->setRequest($request);

        //define what we want the isAuthorized method to return for this method
        $this->user->method('isAuthorized')
            ->will($this->returnCallback(array($this, 'authorizedUser'))); //for an authorized user
        //->will($this->returnCallback(array($this, 'notAuthorizedUser'))); //for unauthorized user testing

        /*** Call the getEmail and get the payload ***/
        try {
            $resp = $this->education->getEducation();
        } catch (\Exception $e) {
            $this->assertEquals($e->getMessage(), 'Pidms must be numeric. (' . $badPidm . ')');
        }
    }

    //helper methods
    public function authorizedUser()
    {
        return true;
    }

    public function notAuthorizedUser()
    {
        return false;
    }

    //private vars
    private $mockDbRecordsSinglePidm = array( //array of 3 database records
        array( //db record 0
            'apradeg_pidm' => '9999436',
            'apradeg_seq_no' => '1',
            'apradeg_sbgi_code' => 'C07104',
            'apradeg_degc_code' => '121',
            'apradeg_date' => '08-MAY-10',
            'apradeg_honr_code' => null,
            'apradeg_coll_code' => 'AP',
            'apradeg_activity_date' => '15-JUN-10',
            'apradeg_acyr_code' => '2010',
            'apradeg_acyr_code_bulletin' => '2010',
            'apradeg_camp_code' => null,
            'apradeg_dept_code' => 'CSA',
            'apradeg_comments' => null,
            'apradeg_majr_code_minr_1' => null,
            'apradeg_majr_code_minr_2' => null,
            'apradeg_majr_code_conc_1' => 'BB',
            'apradeg_majr_code_conc_2' => 'MTH2',
            'apradeg_majr_code_conc_3' => null,
            'apradeg_majr_code_conc_4' => null,
            'apradeg_majr_code_conc_5' => null,
            'apradeg_majr_code_conc_6' => null,
            'apradeg_dept_code_2' => null,
            'apradeg_honr_code_2' => null,
            'apradeg_honr_code_3' => null,
            'apradeg_honr_code_4' => null,
            'apradeg_honr_code_5' => null,
            'apradeg_hond_code_1' => null,
            'apradeg_hond_code_2' => null,
            'apradeg_hond_code_3' => null,
            'apradeg_hond_code_4' => null,
            'apradeg_hond_code_5' => null,
            'apradeg_ok_for_notes_ind' => null,
            'apradeg_disp_notes_ind' => null,
            'apradeg_disp_notes_date' => null,
            'apradeg_user' => 'PROCESS',
            'apradeg_reviewed_ind' => null,
            'apradeg_reviewed_user' => null,
            'apradeg_major_comment' => null),
        array( //db record 1
            'apradeg_pidm' => '9999436',
            'apradeg_seq_no' => '1',
            'apradeg_sbgi_code' => 'C07104',
            'apradeg_degc_code' => '124',
            'apradeg_date' => '08-MAY-12',
            'apradeg_honr_code' => null,
            'apradeg_coll_code' => 'AP',
            'apradeg_activity_date' => '15-JUN-12',
            'apradeg_acyr_code' => '2012',
            'apradeg_acyr_code_bulletin' => '2012',
            'apradeg_camp_code' => null,
            'apradeg_dept_code' => 'CSE',
            'apradeg_comments' => null,
            'apradeg_majr_code_minr_1' => null,
            'apradeg_majr_code_minr_2' => null,
            'apradeg_majr_code_conc_1' => 'BB',
            'apradeg_majr_code_conc_2' => 'MTH2',
            'apradeg_majr_code_conc_3' => null,
            'apradeg_majr_code_conc_4' => null,
            'apradeg_majr_code_conc_5' => null,
            'apradeg_majr_code_conc_6' => null,
            'apradeg_dept_code_2' => null,
            'apradeg_honr_code_2' => null,
            'apradeg_honr_code_3' => null,
            'apradeg_honr_code_4' => null,
            'apradeg_honr_code_5' => null,
            'apradeg_hond_code_1' => null,
            'apradeg_hond_code_2' => null,
            'apradeg_hond_code_3' => null,
            'apradeg_hond_code_4' => null,
            'apradeg_hond_code_5' => null,
            'apradeg_ok_for_notes_ind' => null,
            'apradeg_disp_notes_ind' => null,
            'apradeg_disp_notes_date' => null,
            'apradeg_user' => 'PROCESS',
            'apradeg_reviewed_ind' => null,
            'apradeg_reviewed_user' => null,
            'apradeg_major_comment' => null),
    );
    private $mockDbRecordsMultiPidm = array( //array of 4 database records
        array( //db record 1
            'goremal_pidm' => '1010101',
            'goremal_emal_code' => 'ADM',
            'goremal_email_address' => 'personaltestemail@gmail.com',
            'goremal_status_ind' => 'A',
            'goremal_preferred_ind' => 'N',
            'goremal_activity_date' => '18-MAR-15',
            'goremal_user_id' => 'PROCESS',
            'goremal_comment' => 'comment to add',
            'goremal_disp_web_ind' => 'N'),
        array( //db record 2
            'goremal_pidm' => '1010101',
            'goremal_emal_code' => 'MAE',
            'goremal_email_address' => 'persont@miami.com',
            'goremal_status_ind' => 'A',
            'goremal_preferred_ind' => 'N',
            'goremal_activity_date' => '12-APR-12',
            'goremal_user_id' => 'BANNER',
            'goremal_comment' => 'alumni/invalid email',
            'goremal_disp_web_ind' => 'N'),
        array( //db record 3
            'goremal_pidm' => '1010101',
            'goremal_emal_code' => 'MU',
            'goremal_email_address' => 'persont@miamioh.edu',
            'goremal_status_ind' => 'A',
            'goremal_preferred_ind' => 'N',
            'goremal_activity_date' => '31-OCT-14',
            'goremal_user_id' => 'JONESTE',
            'goremal_comment' => 'main university email address',
            'goremal_disp_web_ind' => 'Y'),
        array( //db record 4
            'goremal_pidm' => '9999436',
            'goremal_emal_code' => 'MU',
            'goremal_email_address' => 'anotherperson@miamioh.edu',
            'goremal_status_ind' => 'A',
            'goremal_preferred_ind' => 'N',
            'goremal_activity_date' => '31-OCT-11',
            'goremal_user_id' => 'JONESTE',
            'goremal_comment' => 'main university email address',
            'goremal_disp_web_ind' => 'Y'),
    );

}
