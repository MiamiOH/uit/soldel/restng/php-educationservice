<?php

/*
-----------------------------------------------------------
FILE NAME: postEducationTest.php

Copyright (c) 2015 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Erin Mills

DESCRIPTION:
This php class is used to test the POST method of the education service.

ENVIRONMENT DEPENDENCIES:
RESTng Framework
PHPUnit
Person/Education

TABLE USAGE:

Web Service Usage:
	Person/Education service (POST)

AUDIT TRAIL:

DATE    PRJ-TSK          UniqueID
Description:

10/14/2015               MILLSE
Description:  Initial Draft

-----------------------------------------------------------
 */

namespace MiamiOH\PhpEducationService\Tests\Unit;

class PostEducationTest extends \MiamiOH\RESTng\Testing\TestCase
{

    private $education, $user, $dbh;
    private $userName = 'testUser';

    /**
     * This setUp method is run before every test method.
     * It sets up a mock API, DBH and DB object with methods that the education class uses
     * and injects those into the Education object.
     */
    protected function setUp()
    {

        //Create the mock API object and define its newResponse method
        $api = $this->getMockBuilder('\MiamiOH\RESTng\Util\API')
            //->setMethods(array('newResponse', 'callResource', 'locationFor'))
            ->setMethods(array('newResponse'))
            ->getMock();
        $api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array', 'perform'))
            ->getMock();

        //set up the mock database object and its getHandle method
        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();
        $db->method('getHandle')->willReturn($this->dbh);

        //set up the mock user
        $this->user = $this->getMockBuilder('\MiamiOH\RESTng\Util\User')
            ->setMethods(array('isAuthorized', 'getUsername'))
            ->getMock();
        $this->user->method('getUsername')->willReturn($this->userName);

        //Create the education object we will be unit testing and pass it the mock objects
        $this->education = new \MiamiOH\PhpEducationService\Services\Education();
        $this->education->setDatabase($db);
        $this->education->setApiUser($this->user);

    }

    // public function testTest(){
    // 	$this->assertEquals('tada', 'tada');
    // }

    public function testCreateEducationSuccess()
    {
        /*** set up to call the createEducation method ***/
        //Create the mock request object, define it's getOptions method and pass it into the education object.
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getData'))
            ->getMock();
        $request->expects($this->once())->method('getData')->willReturn($this->mockDataRecordSuccess);
        $this->education->setRequest($request);

        //define what we want the isAuthorized method to return for this method
        $this->user->method('isAuthorized')
            ->will($this->returnCallback(array($this, 'authorizedUser')));
        //->will($this->returnCallback(array($this, 'notAuthorizedUser'))); //for unauthorized user testing

        $this->dbh->method('perform')->willReturn(true);
        $this->dbh->method('queryall_array')->will($this->returnCallback(array($this, 'queryallArrayResultsNull')));

        /*** Call the getEducation and get the payload ***/
        $resp = $this->education->createEducation();
        $payload = $resp->getPayload();

        /*** Make assertions about the response from the createEducation method ***/
        $this->assertEquals($payload[0]['pidm'], '9999436');
        $this->assertEquals($payload[0]['code'], 201);
        $this->assertEquals($payload[0]['message'], 'Created successfully.');
    }

    public function testCreateEducationFailInsertError()
    {
        /*** set up to call the createEducation method ***/
        //Create the mock request object, define it's getOptions method and pass it into the education object.
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getData'))
            ->getMock();
        $request->expects($this->once())->method('getData')->willReturn($this->mockDataRecordFail);
        $this->education->setRequest($request);

        //define what we want the isAuthorized method to return for this method
        $this->user->method('isAuthorized')
            ->will($this->returnCallback(array($this, 'authorizedUser')));
        //->will($this->returnCallback(array($this, 'notAuthorizedUser'))); //for unauthorized user testing

        $this->dbh->method('perform')->willReturn(false);
        $this->dbh->method('queryall_array')->will($this->returnCallback(array($this, 'queryallArrayResultsNull')));

        /*** Call the createEducation method and get the payload ***/
        $resp = $this->education->createEducation();
        $payload = $resp->getPayload();

        /*** Make assertions about the response from the createEducation method ***/
        $this->assertEquals($payload[0]['pidm'], '9999436');
        $this->assertEquals($payload[0]['code'], 500);
        $this->assertEquals($payload[0]['message'], 'There was a database error when attempting to insert the record.');
    }

    public function testCreateEducationInvalidToken()
    {
        /*** set up to call the createEducation method ***/
        //Create the mock request object, define it's getOptions method and pass it into the education object.
        $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getData'))
            ->getMock();

        $this->education->setRequest($request);

        //define what we want the isAuthorized method to return for this method
        $this->user->method('isAuthorized')
            //->will($this->returnCallback(array($this, 'authorizedUser'))); //for an authorized user
            ->will($this->returnCallback(array($this, 'notAuthorizedUser'))); //for unauthorized user testing

        /*** Call the createEducation and get the payload ***/
        $resp = $this->education->createEducation();

        /*** Make assertions ***/
        $this->assertEquals(\MiamiOH\RESTng\App::API_UNAUTHORIZED, $resp->getStatus());
    }

    //helper methods
    public function authorizedUser()
    {
        return true;
    }

    public function notAuthorizedUser()
    {
        return false;
    }

    public function queryallArrayResults()
    {
        return "stuff";
    }

    public function queryallArrayResultsNull()
    {
        return null;
    }

    //private vars
    private $mockDataRecordSuccess =
        array( //array of input data records
            array( //record 1
                'pidm' => '9999436',
                'schoolCode' => 'C07104',
                'degreeCode' => '122',
            ),
        );
    private $mockDataRecordFail =
        array( //array of 3 database records
            array( //record 1
                'pidm' => '9999436',
                'schoolCode' => 'C07104',
                'degreeCode' => '121',
            ),
        );
}
