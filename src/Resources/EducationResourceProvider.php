<?php

namespace MiamiOH\PhpEducationService\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class EducationResourceProvider extends ResourceProvider
{


    public function registerDefinitions(): void
    {

        $this->addDefinition(array(
            'name' => 'Person.Education.PostResponse',
            'type' => 'object',
            'properties' => array(
                'pidm' => array(
                    'type' => 'number',
                ),
                'code' => array(
                    'type' => 'string',
                ),
                'message' => array(
                    'type' => 'string',
                ),
            ),
        ));

        $this->addDefinition(array(
            'name' => 'Person.Education.PostResponse.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Person.Education.PostResponse'
            )
        ));

        $this->addDefinition(array(
            'name' => 'Person.Education',
            'type' => 'object',
            'properties' => array(
                'pidm' => array(
                    'type' => 'number',
                ),
                'schoolCode' => array(
                    'type' => 'string',
                ),
                'degreeCode' => array(
                    'type' => 'string',
                ),
                'degreeDate' => array(
                    'type' => 'string',
                ),
                'user' => array(
                    'type' => 'string',
                ),
                'activityDate' => array(
                    'type' => 'string',
                ),

            )
        ));

        $this->addDefinition(array(
            'name' => 'Person.Education.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Person.Education'
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(

            'name' => 'Education',
            'class' => 'MiamiOH\PhpEducationService\Services\Education',
            'description' => 'Information about a persons education',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
                'configuration' => array('type' => 'service', 'name' => 'APIConfiguration'),
                'dataSource' => array('type' => 'service', 'name' => 'APIDataSourceFactory'),

            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(

                'action' => 'create',
                'name' => 'person.education.post',
                'description' => 'Create new education record',
                'pattern' => '/person/education/v1',
                'service' => 'Education',
                'method' => 'createEducation',
                'tags' => array('Person'),
                'middleware' => array(
                    'authenticate' => array('type' => 'token'),
                ),
                'returnType' => 'collection',
                'body' => array(
                    'description' => 'An education object',
                    'required' => true,
                    'schema' => array(
                        '$ref' => '#/definitions/Person.Education.Collection'
                    )
                ),
                'responses' => array(
                    App::API_CREATED => array(
                        'description' => 'A collection of education objects',
                        'returns' => array(
                            'type' => 'model',
                            '$ref' => '#/definitions/Person.Education.PostResponse.Collection',
                        )
                    ),
                )
            )
        );

        $this->addResource(array(

                'action' => 'read',
                'name' => 'person.education.get',
                'description' => "Read a person's education records",
                'pattern' => '/person/education/v1',
                'service' => 'Education',
                'method' => 'getEducation',
                'isPageable' => false,
                'tags' => array('Person'),
                'returnType' => 'collection',
                'options' => array(
                    'pidm' => array('type' => 'list', 'description' => 'use pidm to get education records'),
                ),
                'middleware' => array(
                    'authenticate' => array('type' => 'token'),
                ),
                'responses' => array(
                    App::API_OK => array(
                        'description' => 'A collection of education objects',
                        'returns' => array(
                            'type' => 'array',
                            '$ref' => '#/definitions/Person.Education.Collection',
                        )
                    ),
                )
            )
        );

    }

    public function registerOrmConnections(): void
    {

    }
}