<?php
/*
-----------------------------------------------------------
FILE NAME: Education.class.php

Copyright (c) 2015 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Erin Mills

DESCRIPTION:  The education service is designed to get, and insert education information.

Authorization: authentication and authorization are handled by authorization token.

Protected group: SPBPERS_CONFID_IND = 'Y'. We will not get any data for these people.

INPUT:
PARAMETERS: pidm for the get method

ENVIRONMENT DEPENDENCIES: RESTNG FRAMEWORK

TABLE USAGE:
	ALUMNI.APRADEG (SELECT, INSERT)
	SATURN.SPBPERS (SELECT)

AUDIT TRAIL:

DATE    PRJ-TSK          UniqueID
Description:

10/28/2015               MILLSE
Description:  Initial Program

 */

namespace MiamiOH\PhpEducationService\Services;

class Education extends \MiamiOH\RESTng\Service
{

    private $dataSource = '';
    private $configuration = '';
    private $datasource_name = 'MUWS_SEC_PROD';

    //	Helper functions that were called by the frame work and create internal datasource and configuration objects
    public function setDataSource($datasorce)
    {
        $this->dataSource = $datasorce;
    }

    public function setDatabase($database)
    {
        $this->database = $database;
    }

    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
    }

    /*
     * Get education records for a pidm
     */
    public function getEducation()
    {

        //log
        $this->log->debug('  Education:getEducation() was called.');

        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();
        $payload = array();
        $pidms = array();

        //Confirm User Can actually Access this Information
        $user = $this->getApiUser();
        $authorized = $user->isAuthorized('WebServices', 'Person', 'All');
        if (!$authorized) {
            $authorized = $user->isAuthorized('WebServices', 'Person', 'view');
        }

        if (!$authorized) {
            $response->setStatus(\MiamiOH\RESTng\App::API_UNAUTHORIZED);
            return $response;
        }

        //get parameters from user input (url)
        if (isset($options['pidm']) && is_array($options['pidm']) && count($options['pidm']) > 0) { //if pidms are given
            $dbh = $this->database->getHandle($this->datasource_name);


            $intPidms = '';
            foreach ($options['pidm'] as $p) {
                $pidmIsNumeric = is_numeric($p);
                if ($pidmIsNumeric) {
                    $intPidms = $intPidms . $p . ',';
                } else {
                    throw new \Exception('Pidms must be numeric. (' . $p . ')');
                }
            }
            $intPidms = substr($intPidms, 0, strlen($intPidms) - 1);

            $queryString = "select
							  apradeg_pidm,
							  apradeg_seq_no,
							  apradeg_sbgi_code,
							  apradeg_degc_code,
								apradeg_user,
								apradeg_activity_date
							from apradeg, spbpers
							where apradeg_pidm in (" . $intPidms . ")
							and spbpers_pidm = apradeg_pidm
							and (spbpers_confid_ind = 'N' or spbpers_confid_ind is null)
							order by apradeg_pidm";

            $results = $dbh->queryall_array($queryString);
            $payload = $this->buildGetQueryResults($results);
            //$payload = array("this is working");

        } else {
            throw new \Exception('Error getting options or parameter: No pidms or uniqueids are available.');
        }

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);
        return $response;
    }

    public function createEducation()
    {
        //log
        $this->log->debug('  Education:createEducation() was called.');

        //get the response and request objects
        $response = $this->getResponse();
        $request = $this->getRequest();
        $incomingData = $request->getData();
        $payload = array();

        //set up some tracking variables
        $successfulPidms = array();
        $failurePidms = array();
        $successCount = 0;
        $failureCount = 0;
        $report = array();

        //Confirm User Can actually Access this Information
        $user = $this->getApiUser();
        $authorized = $user->isAuthorized('WebServices', 'Person', 'All');
        if (!$authorized) {
            $authorized = $user->isAuthorized('WebServices', 'Person', 'view');
        }

        if (!$authorized) {
            $response->setStatus(\MiamiOH\RESTng\App::API_UNAUTHORIZED);
            return $response;
        }

        $dbh = $this->database->getHandle($this->datasource_name);

        foreach ($incomingData as $eduRecord) {
            //pull out the important data for later use:
            $pidm = $eduRecord['pidm'];
            $schoolCode = $eduRecord['schoolCode'];
            $degreeCode = $eduRecord['degreeCode'];

            //eventually, check for duplicates before inserting

            if (is_null($pidm) || is_null($schoolCode) || is_null($degreeCode) || $pidm == '' || $schoolCode == '' || $degreeCode == '') {
                $report[] = array('pidm' => $pidm, 'code' => \MiamiOH\RESTng\App::API_FAILED, 'message' => "Pidm, school code and degree code must all be given.");
                //array_push($failurePidms, array('pidm' => $pidm, 'message' => 'Pidm, school code and degree code must all be given.'));
                //$failureCount++;
            } else if (!is_numeric($pidm)) {
                $report[] = array('pidm' => $pidm, 'code' => \MiamiOH\RESTng\App::API_FAILED, 'message' => "Pidm must be numeric.");
                //array_push($failurePidms, array('pidm' => $pidm, 'message' => 'Pidm must be numeric.'));
                //$failureCount++;
            } else {
                //construct the binding array
                $parameters = array($pidm, $pidm, $schoolCode, $degreeCode, $this->getApiUser()->getUsername());

                //now, insert the record.
                $insertQuery = "insert into apradeg (
									apradeg_pidm,
									apradeg_seq_no,
									apradeg_sbgi_code,
									apradeg_degc_code,
									apradeg_user,
									apradeg_activity_date)
								values (?, (select coalesce(max(apradeg_seq_no),0)+1 from apradeg where apradeg_pidm = ?), ?, ?, ?, sysdate)";
                $results = $dbh->perform($insertQuery, $parameters);

                if ($results == true) {//if there is no error, push the pidm onto the success stack.
                    $report[] = array('pidm' => $pidm, 'code' => \MiamiOH\RESTng\App::API_CREATED, 'message' => "Created successfully.");
                    //array_push($successfulPidms, $pidm);
                    //$successCount++;
                } else { //if there is an error after the insert, then push the pidm onto the failure stack.
                    $report[] = array('pidm' => $pidm, 'code' => \MiamiOH\RESTng\App::API_FAILED, 'message' => "There was a database error when attempting to insert the record.");
                    //array_push($failurePidms, array('pidm'=>$pidm, 'message'=>'There was a database error when attempting to insert the record.'));
                    //$failureCount++;
                }
            }
        }

// 		array_push($payload,
// 			array('success' => $successfulPidms,
// 				'failure' => $failurePidms,
// 				'countSuccess' => $successCount,
// 				'countFailure' => $failureCount
// 			)
// 		);

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($report);
        return $response;
    }

    //this method takes the input $results from the database call and shoves them into
    //a prettier array with better names for display to the consumer
    private function buildGetQueryResults($results)
    {
        $fancyResults = array();
        foreach ($results as $row) {
            $fancyRow = array();
            $fancyRow['pidm'] = $row['apradeg_pidm'];
            $fancyRow['schoolCode'] = $row['apradeg_sbgi_code'];
            $fancyRow['degreeCode'] = $row['apradeg_degc_code'];
     //       $fancyRow['degreeDate'] = $row['apradeg_date'];
            $fancyRow['user'] = $row['apradeg_user'];
            $fancyRow['activityDate'] = $row['apradeg_activity_date'];

            $fancyResults[] = $fancyRow;
        }
        return $fancyResults;
    }

}

//other fields in the apradeg table that are not being returned or inserted:
// apradeg_date,
// apradeg_honr_code,
// apradeg_coll_code,
// apradeg_activity_date,
// apradeg_acyr_code,
// apradeg_acyr_code_bulletin,
// apradeg_camp_code,
// apradeg_dept_code,
// apradeg_comments,
// apradeg_majr_code_minr_1,
// apradeg_majr_code_minr_2,
// apradeg_majr_code_conc_1,
// apradeg_majr_code_conc_2,
// apradeg_majr_code_conc_3,
// apradeg_majr_code_conc_4,
// apradeg_majr_code_conc_5,
// apradeg_majr_code_conc_6,
// apradeg_dept_code_2,
// apradeg_honr_code_2,
// apradeg_honr_code_3,
// apradeg_honr_code_4,
// apradeg_honr_code_5,
// apradeg_hond_code_1,
// apradeg_hond_code_2,
// apradeg_hond_code_3,
// apradeg_hond_code_4,
// apradeg_hond_code_5,
// apradeg_ok_for_notes_ind,
// apradeg_disp_notes_ind,
// apradeg_disp_notes_date,
// apradeg_user,
// apradeg_reviewed_ind,
// apradeg_reviewed_user,
// apradeg_major_comment
